package hospital_test.hospital_test;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;


public class Setup {
	
	AndroidDriver<MobileElement> driver;

	@BeforeTest
	public void installApp() throws MalformedURLException {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "7GT7N18913004252");
		cap.setCapability(MobileCapabilityType.APP, "C:\\Users\\aadit\\Downloads\\app-release.apk");
		cap.setCapability("autoGrantPermissions", true);
		URL url= new URL("http://0.0.0.0:4723/wd/hub");
		driver= new AndroidDriver(url, cap);
		
		if(!driver.isAppInstalled("com.sensynehealth.hospitals"))
		{
			driver.installApp("C:\\Users\\aadit\\Downloads\\app-release.apk");
			System.out.println("App was not installed on device, installation successful");
			
		}
		else
		{
			driver.removeApp("com.sensynehealth.hospitals");
			System.out.println("App was installed, Uninstalled now");
			driver.installApp("C:\\Users\\aadit\\Downloads\\app-release.apk");
			System.out.println("App installed Successfully");
		}
	}
	
	@Test
	public void performSearch() {
		driver.launchApp();
		String searchIcon = "com.sensynehealth.hospitals:id/search_button";
		driver.findElement(By.id(searchIcon)).click();
		String searchSpace = "com.sensynehealth.hospitals:id/search_src_text";
		driver.findElement(By.id(searchSpace)).sendKeys("east");
		
	}
}
